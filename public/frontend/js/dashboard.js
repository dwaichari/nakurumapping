var map;
var marker;
var myLatlng;
var geocoder = new google.maps.Geocoder();
var infowindow = new google.maps.InfoWindow();

//script for adding a map listing
function initialize() {

    //the following code is for getting the coordinates for loading the map of a listing
    var businessinformation_id = $('#businessinformation_id').val();
    var op = " ";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'post',
        url: '/dashboard-get-listing-coordinates',
        data: {'id': businessinformation_id},
        success: function (data) {
            console.log(data);
            var myLatLng = new google.maps.LatLng(data.latitude, data.longitude);
            createListingMap(myLatLng);
        },
        error: function () {
        }
    });
    //for editing the map
    $.ajax({
        type: 'post',
        url: '/dashboard-get-listing-coordinates',
        data: {'id': businessinformation_id},
        success: function (data) {
            console.log(data);
            var myLatLng = new google.maps.LatLng(data.latitude, data.longitude);
            editListingMap(myLatLng);
        },
        error: function () {
        }
    });

    geoLocationInit();
}

function createMap(myLatlng) {
    var mapOptions = {
        zoom: 15,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("add_listing_map"), mapOptions);

    marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
        draggable: true
    });

    geocoder.geocode({'latLng': myLatlng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                $('#latitude,#longitude').show();
                $('#address').val( results[0].formatted_address);
                $('#latitude').val(marker.getPosition().lat());
                $('#longitude').val(marker.getPosition().lng());
                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
            }
        }
    });

    google.maps.event.addListener(marker, 'dragend', function () {

        geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(+marker.getPosition().lng());
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });
    });

}

function editListingMap(myLatlng) {
    var mapOptions = {
        zoom: 15,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("edit_listing_map"), mapOptions);

    marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
        draggable: true
    });
    google.maps.event.addListener(marker, 'dragend', function () {

        geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(+marker.getPosition().lng());
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });
    });

}


function createListingMap(myLatlng) {
    var mapOptions = {
        zoom: 15,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("show_listing_map"), mapOptions);

    marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
        draggable: false
    });

}

function geoLocationInit() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, fail);
    } else {
        alert("Browser not supported");
    }

}

function success(position) {
    console.log(position);
    var latval = position.coords.latitude;
    var lngval = position.coords.longitude;
    myLatLng = new google.maps.LatLng(latval, lngval);
    createMap(myLatLng);
}

function fail() {
    alert("it fails");
}

google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, 'load', initialize);

//the following code is for populating the wards based on the subcounty selected
$(document).on('change', '#listing_subcounty', function () {
    var subcounty_id = $(this).val();
    //console.log(cat_id);
    var op = " ";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: '/get-subcounty-wards',
        data: {'id': subcounty_id},
        success: function (data) {
            op += '<option value="0" selected disabled>Select your Ward</option>';
            for (var i = 0; i < data.length; i++) {
                op += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }
            $('#listing_ward').html('');
            $('#listing_ward').append(op);
        },
        error: function () {

        }
    });
});

//the following code is for setting the coordinates of the map
$(document).on('change', '#listing_ward', function () {
    var ward_id = $(this).val();
    //console.log(cat_id);
    var op = " ";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: '/get-ward-coordinates',
        data: {'id': ward_id},
        success: function (data) {
            var myLatLng = new google.maps.LatLng(data.latitude, data.longitude);
            createMap(myLatLng);
        },
        error: function () {

        }
    });
});

//the following code is for setting the coordinates of the map for editing
$(document).on('change', '#listing_ward', function () {
    var ward_id = $(this).val();
    //console.log(cat_id);
    var op = " ";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: '/get-ward-coordinates',
        data: {'id': ward_id},
        success: function (data) {
            var myLatLng = new google.maps.LatLng(data.latitude, data.longitude);
            editListingMap(myLatLng);
        },
        error: function () {

        }
    });
});

//the following code is for populating the sub categories based on the category selected
$(document).on('change', '#listing_category', function () {
    var category_id = $(this).val();
    //console.log(cat_id);
    var op = " ";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: '/get-subcategories',
        data: {'id': category_id},
        success: function (data) {
            op += '<option value="0" selected disabled>Select a Sub Category</option>';
            for (var i = 0; i < data.length; i++) {
                op += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }
            $('#listing_subcategory').html('');
            $('#listing_subcategory').append(op);
        },
        error: function () {

        }
    });
});

//end of add listing script

//code for populating a modal for editing

$('#editBusinessDay').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var day = button.data('day');
    var from = button.data('from');
    var to = button.data('to');
    var id = button.data('id');
    var listing_id = button.data('listing_id');
    var url = button.data('url');
    var modal = $(this);
    modal.find('.modal-body #day').val(day);
    modal.find('.modal-body #from').val(from);
    modal.find('.modal-body #to').val(to);
    modal.find('.modal-body #id').val(id);
    modal.find('.modal-body #listing_id').val(listing_id);
    modal.find('.modal-body #editBusinessDay').attr('action', url);

})
//for deleting a business hour
$('#deleteBusinessDay').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var url = button.data('url');
    var modal = $(this);
    modal.find('.modal-body #deleteid').val(id);
    modal.find('.modal-body #deleteBusinessDay').attr('action', url);

})

