<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('listings-map/{id}','HomeController@listingsMap')->name('listings.map');
Route::get('listing-details/{id}','HomeController@listingDetails')->name('listing.details');

Route::get('/dashboard','DashboardController@dashboard')->name('dashboard');
Route::get('/dashboard-all-listing','DashboardController@allListing')->name('dashboard.alllistings');
Route::get('/dashboard-add-listing','DashboardController@addListing')->name('dashboard.addlisting');
Route::get('/dashboard-show-listing/{id}','DashboardController@showListing')->name('dashboard.showListing');
Route::post('/dashboard-get-listing-coordinates','DashboardController@getListingCoordinates')->name('dashboard.getListingCoordinates');
Route::post('/dashboard-get-edit-listing-coordinates','DashboardController@getEditListingCoordinates')->name('dashboard.getEditListingCoordinates');
Route::post('/dashboard-store-listing','DashboardController@storeListing')->name('dashboard.storeListing');
Route::get('/dashboard-edit-listing/{id}','DashboardController@editListing')->name('dashboard.editListing');
Route::post('/get-subcounty-wards','DashboardController@getWards');
Route::post('/get-ward-coordinates','DashboardController@getWardCoordinates');
Route::post('/get-subcategories','DashboardController@getSubCategories');
