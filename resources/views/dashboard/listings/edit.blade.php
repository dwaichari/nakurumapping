@extends('layouts.dashboard')
@section('title','ListingGEO - Add-Listing')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-block">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item"><a href="single-listing.html">Listing </a></li>
                    <li class="breadcrumb-item"><a href="single-listing.html">Edit </a></li>
                    <li class="breadcrumb-item active">{!! $businessinformation->name !!}</li>
                </ol>
                <div class="breadcrumb-call-to-action">
                    <a href="{!! route('dashboard.editListing',$businessinformation->id) !!}" class="listing-btn-large">
                        Save Changes
                    </a>
                </div>
            </div>
        </div>
        <form id="add_listing_form" class="form-common add-listing-form" action="#" method="post" novalidate="novalidate" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="listing-block businessinfo-listing">
                <input type="hidden" value="{!! $businessinformation->id !!}" name="businessinformation_id" id="businessinformation_id">
                <h4>Business Information</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="businessinfo-left-block">
                            <div class="form-group">
                                <label for="listing_name">Listing Name</label>
                                <input id="listing_name" type="text" class="form-control form-single-element"
                                       aria-required="true" name="name" value="{!! $businessinformation->name !!}">
                                <input id="listing_tagline" type="text" class="form-control"
                                       aria-required="true" name="tagline" value="{!! $businessinformation->tagline !!}">
                            </div>
                            <div class="form-group">
                                <label for="listing_email">Contact Email</label>
                                <input id="listing_email" type="email" class="form-control"
                                       aria-required="true" name="email"  value="{!! $businessinformation->email !!}">
                            </div>
                            <div class="form-group">
                                <label for="listing_phone_no">Contact Phone</label>
                                <input id="listing_phone_no" type="text" class="form-control"
                                       aria-required="true" name="phone"  value="{!! $businessinformation->phone !!}">
                            </div>
                        </div>
                    </div>
                    <!-- col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="businessinfo-right-block">
                            <div class="form-group">
                                <label for="listing_website">Website</label>
                                <input id="listing_website" type="text" class="form-control"
                                       aria-required="true" name="website"  value="{!! $businessinformation->website !!}">
                            </div>
                            <div class="form-group">
                                <label for="listing_subcounty">Sub County</label>
                                <select id="listing_subcounty" class="form-control" name="subcounty_id">
                                    <option value="{!! $businessinformation->subcounty()->pluck('id')->implode('') !!}">{!! $businessinformation->subcounty()->pluck('name')->implode('') !!}</option>
                                    @foreach($subcounties as $subcounty )
                                        <option value="{!! $subcounty->id !!}">{!! $subcounty->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="listing_ward">Ward</label>
                                <select id="listing_ward" class="form-control listing_ward" name="ward_id">
                                    <option value="{!! $businessinformation->ward()->pluck('id')->implode('') !!}">{!! $businessinformation->ward()->pluck('name')->implode('') !!}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <p class="text text-warning text-capitalize text-center">Drag the marker to  the specific location of your listing having selected the ward. <br>
                            You can click on the top right <img id="maximizemap" src="{!! asset('frontend/images/maximizemap.jpg') !!}"> to maximize the map to full page for better viewing and mapping</p>
                        <div id="edit_listing_map" class="add-listing-map"></div>
                        <input id="address" type="text" name="address" class="form-control" placeholder="Address" readonly value="{!! $businessinformation->address !!}">
                        <div class="row">
                            <div class="col-md-5">
                                <input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude" readonly value="{!! $businessinformation->latitude !!}"/>
                            </div>
                            <div class="col-md-5">
                                <input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude" readonly value="{!! $businessinformation->longitude !!}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- businessinfo-listing -->
            <div class="listing-block Others-Information-listing">
                <h4>Other Information</h4>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label for="listing_category">Category</label>
                            <select id="listing_category" class="form-control" name="category_id">
                                <option value="{!! $businessinformation->category()->pluck('id')->implode('')!!}">{!! $businessinformation->category()->pluck('name')->implode('')!!}</option>
                                @foreach($categories as $category)
                                    <option name="category_id" value="{!! $category->id !!}">{!! $category->name !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label for="listing_subcategory">Sub Category</label>
                            <select id="listing_subcategory" class="form-control" name="subcategory_id">
                                <option value="{!! $businessinformation->category()->pluck('id')->implode('')!!}">{!! $businessinformation->subcategory()->pluck('name')->implode('')!!}</option>
                            </select>
                        </div>
                    </div>
                    <hr/>
                    <div class="col-lg-8">
                        <div class="listing-businesshour-block">
                            <h5>Open Business Days and Hours </h5>
                            @if(count($businessdays)>0)
                                <a class="btn btn-primary" data-toggle="modal" data-target="#squarespaceModal" ><span class="">Add</span></a>
                                <div class="form-check">
                                    @foreach($businessdays as $businessday)
                                        <div class="multiple-label-checkbox-block">
                                            <p><strong>{!! $businessday->day !!} [+]</strong> {!! $businessday->from !!} <b>-</b> {!! $businessday->to !!}</p>
                                            <span class="custom-control-indicator"></span>
                                            <button onclick="event.preventDefault();" class="btn btn-sm btn-warning"  data-toggle="modal"
                                                    data-target="#editBusinessDay" data-day="{{$businessday->day}}" data-from="{{$businessday->from}}"
                                                    data-to="{{$businessday->to}}" data-listing_id={{$businessday->listing_id}}
                                                    data-id={!! $businessday->id !!} data-url="/admin/update-business-day/{!! $businessday->id !!}" >
                                                <span class="glyphicon glyphicon-edit">Edit</span></button>   |
                                            <button onclick="event.preventDefault();" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteBusinessDay"
                                                    data-url="/admin/delete-business-day/{!! $businessday->id !!}" data-id={{$businessday->id}}><span class="glyphicon glyphicon-trash">Delete</span></button>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                <div class="alert alert-warning" role="alert">
                                    <p>You have not added any business day!</p>
                                    <a class="btn btn-primary" data-toggle="modal" data-target="#squarespaceModal" ><span class="">Add</span></a>
                                </div>
                            @endif
                        </div>
                        <!-- listing-businesshour-block -->
                    </div>
                </div>
            </div>
            <!-- Others-Information-listing -->
            <div class="listing-block details-listing">
                <h4>Listing Details</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group listing-compose-block">
                            <label for="listing_description">Description</label>
                            @if($listingdetails->description !='')
                                <textarea rows="5" cols="50" id="listing_description" name="description" class="form-control"> {!! $listingdetails->description !!} </textarea>
                            @else
                                <textarea rows="5" cols="50" id="listing_description" name="description" class="form-control" placeholder="Add a Description"></textarea>
                            @endif
                        </div>
                        <div class="social-profile-block form-group">
                            <label for="listing_facebook_url">Social Profile</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        @if( $listingdetails->facebookurl !='')
                                            <input id="listing_facebook_url" type="text" name="facebookurl" class="form-control" value="{!! $listingdetails->facebookurl !!}" aria-required="true">
                                        @else
                                            <input id="listing_facebook_url" type="text" name="facebookurl" class="form-control text " placeholder="Add a Facebook link" aria-required="true">
                                        @endif

                                    </div>
                                    <div class="form-group">
                                        @if($listingdetails->twitterurl !='')
                                            <input id="listing_twitter_url" type="text" name="twitterurl" class="form-control" value="{!! $listingdetails->twitterurl !!}" aria-required="true">
                                        @else
                                            <input id="listing_twitter_url" type="text" name="twitterurl" class="form-control " placeholder="Add a  Twitter Link" aria-required="true">
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        @if($listingdetails->instagramurl != '')
                                            <input id="listing_instagram_url" type="text" name="instagramurl" class="form-control" value="{!! $listingdetails->instagramurl !!}" aria-required="true">
                                        @else
                                            <input id="listing_instagram_url" type="text" name="instagramurl" class="form-control " placeholder="Add an Instagram Link" aria-required="true">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        @if($listingdetails->googleplusurl !='')
                                            <input id="listing_googleplus_url" type="text" name="googleplusurl" class="form-control" value="{!! $listingdetails->googleplusurl !!}" aria-required="true">
                                        @else
                                            <input id="listing_googleplus_url" type="text" name="googleplusurl" class="form-control" placeholder="Add a  Google Plus Link" aria-required="true">
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        @if($listingdetails->youtubeurl != '')
                                            <input id="listing_youtube_url" type="text" name="youtubeurl" class="form-control" value="{!! $listingdetails->youtubeurl !!}" aria-required="true">
                                        @else
                                            <input id="listing_youtube_url" type="text" name="youtubeurl" class="form-control text " placeholder="Add a  Youtube Channel Link" aria-required="true">
                                        @endif

                                    </div>
                                    <div class="form-group">
                                        @if($listingdetails->linkedinurl !='')
                                            <input id="listing_linkedin_url" type="text" name="linkedinurl"class="form-control" value="{!! $listingdetails->linkedinurl !!}" aria-required="true">
                                        @else
                                            <input id="listing_linkedin_url" type="text" name="linkedinurl" class="form-control" placeholder="Add a  Linkedin Link" aria-required="true">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotional-video-block form-group">
                            <label for="listing_youtube_video_1">Promotional Video</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        @if($listingdetails->youtubevidone !='')
                                            <input id="listing_youtube_video_1" name="youtubevidone" type="text" class="form-control" value="{!! $listingdetails->youtubevidone !!}" aria-required="true">
                                        @else
                                            <input id="listing_youtube_video_1" name="youtubevidone" type="text" class="form-control " placeholder="YouTube Video URL Here" aria-required="true">
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        @if($listingdetails->youtubevidtwo != '')
                                            <input id="listing_youtube_video_2" name="youtubevidtwo" type="text" class="form-control"value="{!! $listingdetails->youtubevidtwo !!}" aria-required="true">
                                        @else
                                            <input id="listing_youtube_video_2" name="youtubevidtwo" type="text" class="form-control" placeholder="YouTube Video URL Here" aria-required="true">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        @if($listingdetails->youtubevidthree !='')
                                            <input id="listing_youtube_video_3" name="youtubevidthree" type="text" class="form-control" value="{!! $listingdetails->youtubevidthree !!}" aria-required="true">
                                        @else
                                            <input id="listing_youtube_video_3" name="youtubevidthree" type="text" class="form-control " placeholder="YouTube Video URL Here" aria-required="true">
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        @if($listingdetails->youtubevidfour !='')
                                            <input id="listing_youtube_video_4" name="youtubevidfour" type="text" class="form-control" value="{!! $listingdetails->youtubevidfour !!}" aria-required="true">
                                        @else
                                            <input id="listing_youtube_video_4" name="youtubevidfour" type="text" class="form-control " placeholder="YouTube Video URL Here" aria-required="true">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group textarea-form-group">
                            <label for="listing_kewords">Keywords</label>
                            @if($listingdetails->keywords !='')
                                <textarea rows="5" cols="50" id="listing_kewords" name="listing_kewords" class="form-control" >{!! $listingdetails->keywords !!}</textarea>
                            @else
                                <textarea rows="5" cols="50" id="listing_kewords" name="listing_kewords" class="form-control" placeholder="Comma Seperated Ex:  Real Estate, Construction" ></textarea>
                            @endif
                        </div>
                        <div class="uload-images-block">
                            <h5>Uploaded Images</h5>
                            <div class="form-group form-group-file-type">
                                <label for="listing_company_logo">Company Logo</label>
                                <input type="file" class="form-control-file" id="listing_company_logo" name="listing_company_logo">
                                @if($businessinformation->companylogo()->pluck('name')->implode('') !='noimage.jpg')
                                    <span class="text text-success">{!! $businessinformation->companylogo()->pluck('name')->implode('') !!}</span>
                                @else
                                    <span>Maximum file size: 20 MB.</span>
                                @endif
                            </div>
                            <div class="form-group form-group-file-type">
                                <label for="listing_company_cover_photo">Cover Image</label>
                                <input type="file" class="form-control-file" id="listing_company_cover_photo" name="listing_company_cover_photo">
                                @if($businessinformation->coverimage()->pluck('name')->implode('') !='noimage.jpg')
                                    <span class="text text-success">{!! $businessinformation->coverimage()->pluck('name')->implode('') !!}</span>
                                @else
                                    <span>Maximum file size: 20 MB.</span>
                                @endif
                            </div>
                            <div class="form-group form-group-file-type">
                                <label for="listing_company_slide_photo">Slide Show Images</label>
                                <input type="file" class="form-control-file" id="listing_company_slide_photo" name="listing_company_slide_photo[]" multiple>
                                @if($businessinformation->slideshows()->pluck('name')->implode('') !='noimage.jpg')
                                    <span class="text text-success">You have slideshows</span>
                                @else
                                    <span>Maximum file size: 90 MB.</span>
                                @endif
                            </div>
                            <div class="form-group form-group-file-type">
                                <label for="listing_company_gallary_photo">Gallery Images (optional)</label>
                                <input type="file" class="form-control-file" id="listing_company_gallary_photo" name="listing_company_gallery_photo[]" multiple>
                                @if($businessinformation->galleries()->pluck('name')->implode('') !='noimage.jpg')
                                    <span class="text text-success">You have a gallery</span>
                                @else
                                    <span>Maximum file size: 90 MB.</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <h4>Frequently Asked Questions (F.A.Qs)</h4>
                        @if(count($faqs) >0)
                            <table class="table table-sm table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Question</th>
                                    <th scope="col">Answer</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($faqs as $faq)
                                    <tr>
                                        <td>{!! $faq->id !!}</td>
                                        <td >{!! $faq->question !!}</td>
                                        <td>{!! $faq->answer !!}</td>
                                        <td>Edit</td>
                                        <td>Delete</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Question</th>
                                    <th scope="col">Answer</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                </tr>
                                </tfoot>
                            </table>
                            <button type="button"  class="btn btn-primary" data-toggle="modal" data-target="#addFaq">+ Add New</button>
                        @else
                            <div class="alert alert-warning" role="alert">
                                <p>You have not added any frequently asked questions!</p>
                                <button type="button"  class="btn btn-primary" data-toggle="modal" data-target="#addFaq">+ Add New</button>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
            <!-- details-listing -->
            <div class="listing-block attach-document-listing">
                <h4>Attached Documents</h4>
                <div class="form-group form-group-file-type">
                    <label for="listing_company_profile">Company Profile</label>
                    <input type="file" class="form-control-file" id="listing_company_profile" name="listing_company_profile">
                    @if($businessinformation->companyprofile()->pluck('name')->implode('') !='noimage.jpg')
                        <span class="text text-success">{!! $businessinformation->companyprofile()->pluck('name')->implode('') !!}</span>
                    @else
                        <span>File Format .pdf, .doc, .docx, .ppt, .pptx, .jpeg, .png</span>
                    @endif
                </div>
                <div class="form-group form-group-file-type">
                    <label for="listing_company_brochure">Company Brochure</label>
                    <input type="file" class="form-control-file" id="listing_company_brochure" name="listing_company_brochure">
                    @if($businessinformation->companybronchure()->pluck('name')->implode('') !='noimage.jpg')
                        <span class="text text-success">{!! $businessinformation->companybronchure()->pluck('name')->implode('') !!}</span>
                    @else
                        <span>File Format .pdf, .doc, .docx, .ppt, .pptx, .jpeg, .png</span>
                    @endif
                </div>
                <div class="form-group form-group-file-type">
                    <label for="listing_company_resturant">Food Menu (Restaurant)</label>
                    <input type="file" class="form-control-file" id="listing_company_resturant" name="listing_company_menu">
                    @if($businessinformation->menu()->pluck('name')->implode('') !='noimage.jpg')
                        <span class="text text-success">{!! $businessinformation->menu()->pluck('name')->implode('') !!}</span>
                    @else
                        <span>File Format .pdf, .doc, .docx, .ppt, .pptx, .jpeg, .png</span>
                    @endif
                </div>
                <div class="form-group form-group-file-type">
                    <label for="listing_company_document">Others Document</label>
                    <input type="file" class="form-control-file" id="listing_company_document" name="listing_company_document">
                    @if($businessinformation->other()->pluck('name')->implode('') !='noimage.jpg')
                        <span class="text text-success">{!! $businessinformation->other()->pluck('name')->implode('') !!}</span>
                    @else
                        <span>File Format .pdf, .doc, .docx, .ppt, .pptx, .jpeg, .png</span>
                    @endif
                </div>
            </div>
            <!-- attach-document-listing -->
            <div class="listing-block prview-listing">
                <h4 class="text-center">To Review your listing click the button below</h4>
                <button type="submit" class="listing-btn-large">Save Changes & Preview</button>
            </div>
            <!-- prview-listing -->
        </form>
    </div>
@endsection