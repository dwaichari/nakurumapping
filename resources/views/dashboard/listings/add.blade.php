@extends('layouts.dashboard')
@section('title','ListingGEO - Add-Listing')
@section('main-content')
    <div class="container-fluid">
        <form id="add_listing_form" class="form-common add-listing-form" action="{!! route('dashboard.storeListing') !!}" method="post" novalidate="novalidate" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="listing-block businessinfo-listing">
                <h4>Business Information</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="businessinfo-left-block">
                            <div class="form-group">
                                <label for="listing_name">Listing Name</label>
                                <input id="listing_name" type="text" class="form-control form-single-element"
                                       placeholder="Ex: Anderson Hotel" aria-required="true" name="name" value="{!! old('name') !!}">
                                <input id="listing_tagline" type="text" class="form-control"
                                       placeholder="Business Tagline goes here" aria-required="true" name="tagline" >
                            </div>
                            <div class="form-group">
                                <label for="listing_email">Contact Email</label>
                                <input id="listing_email" type="email" class="form-control"
                                       placeholder="Ex: info@example.com" aria-required="true" name="email" value="{!! old('email') !!}">
                            </div>
                            <div class="form-group">
                                <label for="listing_phone_no">Contact Phone</label>
                                <input id="listing_phone_no" type="text" class="form-control"
                                       placeholder="Ex: +1-0000-000-000" aria-required="true" name="phone" value="{!! old('contact') !!}">
                            </div>
                        </div>
                    </div>
                    <!-- col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="businessinfo-right-block">
                            <div class="form-group">
                                <label for="listing_website">Website</label>
                                <input id="listing_website" type="text" class="form-control"
                                       placeholder="Ex: www.example.com" aria-required="true" name="website" value="{!! old('website') !!}">
                            </div>
                            <div class="form-group">
                                <label for="listing_subcounty">Sub County</label>
                                <select id="listing_subcounty" class="form-control" name="subcounty_id">
                                    <option value="">Select your Sub County</option>
                                   @foreach($subcounties as $subcounty )
                                        <option value="{!! $subcounty->id !!}">{!! $subcounty->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="listing_ward">Ward</label>
                                <select id="listing_ward" class="form-control listing_ward" name="ward_id">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <p class="text text-warning text-capitalize text-center">Drag the marker to  the specific location of your listing having selected the ward. <br>
                            You can click on the top right <img id="maximizemap" src="{!! asset('frontend/images/maximizemap.jpg') !!}"> to maximize the map to full page for better viewing and mapping</p>
                        <div id="add_listing_map" class="add-listing-map"></div>
                        <input id="address" type="text" name="address" class="form-control" placeholder="Address" readonly value="{!! old('address') !!}">
                        <div class="row">
                            <div class="col-md-5">
                                <input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude" readonly value="{!! old('latitude') !!}"/>
                            </div>
                            <div class="col-md-5">
                                <input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude" readonly value="{!! old('longitude') !!}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- businessinfo-listing -->
            <div class="listing-block Others-Information-listing">
                <h4>Other Information</h4>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label for="listing_category">Category</label>
                            <select id="listing_category" class="form-control" name="category_id">
                                <option value="">Select Category</option>
                                @foreach($categories as $category)
                                    <option name="category_id" value="{!! $category->id !!}">{!! $category->name !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label for="listing_subcategory">Sub Category</label>
                            <select id="listing_subcategory" class="form-control" name="subcategory_id">
                            </select>
                        </div>
                    </div>
                    <hr/>
                    <div class="col-lg-8">
                        <div class="listing-businesshour-block">
                            <h5>Open Business Days and Hours </h5>
                            <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#squarespaceModal" ><span class="">Add</span></a>
                            <div class="form-check">
                                @foreach($businessdays as $businessday)
                                    <div class="multiple-label-checkbox-block">
                                        <p><strong>{!! $businessday->day !!} [+]</strong> {!! $businessday->from !!} <b>-</b> {!! $businessday->to !!}</p>
                                        <span class="custom-control-indicator"></span>
                                        <button onclick="event.preventDefault();" class="btn btn-sm btn-warning"  data-toggle="modal"
                                                data-target="#editBusinessDay" data-day="{{$businessday->day}}" data-from="{{$businessday->from}}"
                                                data-to="{{$businessday->to}}" data-listing_id={{$businessday->listing_id}}
                                                data-id={!! $businessday->id !!} data-url="/admin/update-business-day/{!! $businessday->id !!}" >
                                            <span class="glyphicon glyphicon-edit">Edit</span></button>   |
                                        <button onclick="event.preventDefault();" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteBusinessDay"
                                                data-url="/admin/delete-business-day/{!! $businessday->id !!}" data-id={{$businessday->id}}><span class="glyphicon glyphicon-trash">Delete</span></button>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- listing-businesshour-block -->
                    </div>
                </div>
            </div>
            <!-- Others-Information-listing -->
            <div class="listing-block details-listing">
                <h4>Listing Details</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group listing-compose-block">
                            <label for="listing_description">Description</label>
                            <textarea rows="5" cols="50" id="listing_description" class="form-control" name="description" placeholder="Comma Seperated Ex:  Real Estate, Construction"></textarea>
                        </div>
                        <div class="social-profile-block form-group">
                            <label for="listing_facebook_url">Social Profile</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input id="listing_facebook_url" type="text" class="form-control" name="facebookurl" placeholder="Facebook URL" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        <input id="listing_twitter_url" type="text" class="form-control" name="twitterurl" placeholder="Twitter URL" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        <input id="listing_instagram_url" type="text" class="form-control" name="instagramurl" placeholder="Instagram URL" aria-required="true">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input id="listing_googleplus_url" type="text" class="form-control" name="googleplusurl" placeholder="Google Plus URL" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        <input id="listing_youtube_url" type="text" class="form-control" name="youtubeurl" placeholder="YouTube URL" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        <input id="listing_linkedin_url" type="text" class="form-control" name="linkedinurl" placeholder="Linkedin URL" aria-required="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="promotional-video-block form-group">
                            <label for="listing_youtube_video_1">Promotional Video</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input id="listing_youtube_video_1" type="text" name="youtubevidone" class="form-control" placeholder="YouTube Video URL Here" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        <input id="listing_youtube_video_2" type="text" name="youtubevidtwo" class="form-control" placeholder="YouTube Video URL Here" aria-required="true">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input id="listing_youtube_video_3" name="youtubevidthree" type="text" class="form-control" placeholder="YouTube Video URL Here" aria-required="true">
                                    </div>
                                    <div class="form-group">
                                        <input id="listing_youtube_video_4" name="youtubevidfour" type="text" class="form-control" placeholder="YouTube Video URL Here" aria-required="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group textarea-form-group">
                            <label for="listing_kewords">Keywords</label>
                            <textarea rows="5" cols="50" id="listing_kewords" name="listing_kewords" class="form-control" placeholder="Comma Seperated Ex:  Real Estate, Construction"></textarea>
                        </div>
                        <div class="uload-images-block">
                            <h5>Upload Images</h5>
                            <div class="form-group form-group-file-type">
                                <label for="listing_company_logo">Company Logo</label>
                                <input type="file" class="form-control-file" id="listing_company_logo" name="listing_company_logo">
                                <span>Maximum file size: 20 MB.</span>
                            </div>
                            <div class="form-group form-group-file-type">
                                <label for="listing_company_cover_photo">Cover Image</label>
                                <input type="file" class="form-control-file" id="listing_company_cover_photo" name="listing_company_cover_photo">
                                <span>Maximum file size: 20 MB.</span>
                            </div>
                            <div class="form-group form-group-file-type">
                                <label for="listing_company_slide_photo">Slide Show Images</label>
                                <input type="file" class="form-control-file" id="listing_company_slide_photo" name="listing_company_slide_photo[]" multiple>
                                <span>Maximum file size: 90 MB.</span>
                            </div>
                            <div class="form-group form-group-file-type">
                                <label for="listing_company_gallary_photo">Gallery Images (optional)</label>
                                <input type="file" class="form-control-file" id="listing_company_gallary_photo" name="listing_company_gallery_photo[]" multiple>
                                <span>Maximum file size: 90 MB.</span>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <h4>Frequently Asked Questions (F.A.Qs)</h4>
                        <table class="table table-sm table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Question</th>
                                <th scope="col">Answer</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                           @foreach($faqs as $faq)
                                <tr>
                                    <td>{!! $faq->id !!}</td>
                                    <td >{!! $faq->question !!}</td>
                                    <td>{!! $faq->answer !!}</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Question</th>
                                <th scope="col">Answer</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
                <button type="button" class="listing-btn-large" data-toggle="modal" data-target="#addFaq">+ Add New</button>
            </div>
            <!-- details-listing -->
            <div class="listing-block attach-document-listing">
                <h4>Attach Documents</h4>
                <div class="form-group form-group-file-type">
                    <label for="listing_company_profile">Company Profile</label>
                    <input type="file" class="form-control-file" id="listing_company_profile" name="listing_company_profile">
                    <span>File Format .pdf, .doc, .docx, .ppt, .pptx, .jpeg, .png</span>
                </div>
                <div class="form-group form-group-file-type">
                    <label for="listing_company_brochure">Company Brochure</label>
                    <input type="file" class="form-control-file" id="listing_company_brochure" name="listing_company_brochure">
                    <span>File Format .pdf, .doc, .docx, .ppt, .pptx, .jpeg, .png</span>
                </div>
                <div class="form-group form-group-file-type">
                    <label for="listing_company_resturant">Food Menu (Restaurant)</label>
                    <input type="file" class="form-control-file" id="listing_company_resturant" name="listing_company_menu">
                    <span>File Format .pdf, .doc, .docx, .ppt, .pptx, .jpeg, .png</span>
                </div>
                <div class="form-group form-group-file-type">
                    <label for="listing_company_document">Others Document</label>
                    <input type="file" class="form-control-file" id="listing_company_document" name="listing_company_document">
                    <span>File Format .pdf, .doc, .docx, .ppt, .pptx, .jpeg, .png</span>
                </div>
            </div>
            <!-- attach-document-listing -->
            <div class="listing-block prview-listing">
                <h4 class="text-center">To Review your listing click the button below</h4>
                <button type="submit" class="listing-btn-large">Save & Preview</button>
            </div>
            <!-- prview-listing -->
        </form>
    </div>
@endsection