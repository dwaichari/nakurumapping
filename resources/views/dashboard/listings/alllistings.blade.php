@extends('layouts.dashboard')
@section('title','ListingGEO - Dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="row">
            @foreach($businessinformations as $businessinformation)
            <div class="col-lg-4 col-md-6">
                <article class="popular-listing-post">
                    <div class="post-thumb">
                        <img src="{!! asset('frontend/images/post/9.jpg') !!}" alt="img" class="img-responsive">
                        <div class="listing-info">
                            <h4><a href="{!! route('dashboard.showListing',$businessinformation->id) !!}">{!! $businessinformation->name !!}</a></h4>
                            <p><i class="fa fa-bed" aria-hidden="true"></i> {!! $businessinformation->category()->pluck('name')->implode('') !!}</p>
                        </div>
                        <div class="rating-area">
                            <ul>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <span>
										(5.0/4)
									</span>
                        </div>
                        <div class="option-block">
                            <ul>
                                <li>
                                    <a href="javascript:void(0)" class="bookmark">

                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#post_listing_modal_one">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="overlay"></div>
                    </div>
                    <div class="post-details">
                        <div class="post-meta">
                            <div class="location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <h5><a href="javascript:void(0)">California</a></h5>
                            </div>
                            <div class="tag">
                                <span>Ad</span>
                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="post-entry-block">
                            <div class="post-author">
                                <img src="{!! asset('frontend/images/post/author/1.jpg') !!}" alt="img" class="img-responsive">
                            </div>
                            <p class="post-entry">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                            </p>
                        </div>
                        <div class="post-footer">
                            <div class="contact-no">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <h5> +1-0000-000-000</h5>
                            </div>
                            <div class="schedule-info closed">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                <h5>Closed Now</h5>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
                @endforeach
        </div>
    </div>
@endsection