@extends('layouts.user')
@section('title','ListingGEO - Home')
@section('main-content')
    <div class="subheader subheader-two"  @if(    $businessinformation->coverimage()->pluck('name')->implode('') !='noimage.jpg')
    style="background-image: url('/storage/Cover Images/{{$businessinformation->name}}/{{$businessinformation->coverimage()->pluck('name')->implode('')}}')"
         @else
         style ="background-image: url({!! asset('frontend/images/background/4.jpg') !!});"
            @endif>
        <div class="subheader-two-block">
            <div class="thumb">
                @if(    $businessinformation->companylogo()->pluck('name')->implode('') !='noimage.jpg')
                    <img src="/storage/Company Logos/{!! $businessinformation->name !!}/{!! $businessinformation->companylogo()->pluck('name')->implode('')  !!}" alt="img" class="img-responsive">
                @else
                    <img src="/storage/nologo.jpg" alt="img" class="img-responsive">
                @endif

            </div>
            <h2>{!! $businessinformation->name !!}</h2>
            <p>{!! $businessinformation->tagline !!}</p>
            <ul>
                <li>
                    <p>
                        <i class="fa fa-map-marker" aria-hidden="true"></i> {!! $businessinformation->address !!}
                    </p>
                </li>
                <li>
                    <p>
                        <i class="fa fa-phone" aria-hidden="true"></i> {!! $businessinformation->phone !!}
                    </p>
                </li>
                <li>
                    <p>
                        <i class="fa fa-envelope-o" aria-hidden="true"></i> {!! $businessinformation->email !!}
                    </p>
                </li>
            </ul>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="single-post-meta-left">
                        <div class="rating-area">
                            <ul>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <span>
								(5.0/4)
							</span>
                        </div>
                        <div class="write-review">
                            <a href="javascript:void(0)">
                                <i class="fa fa-pencil" aria-hidden="true"></i> Write a Review
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-post-meta-right">
                        <ul class="single-post-meta-list">
                            <li class="have-social-block">
                                <div class="social-share-block">
                                    <button class="social-trigger-btn">
                                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    </button>
                                    <ul class="social-list">
                                        <li>
                                            <a href="{!! $listingdetails->facebookurl!!}">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                <i class="fa fa-google-plus" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="bookmark-block">
                                <a href="javascript:void(0)" class="bookmark">
                                    save
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="icon-btn claim-btn" data-toggle="modal" data-target="#claim-modal">
                                    <i class="fa fa-check" aria-hidden="true"></i> Claim
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="icon-btn message-btn" data-toggle="modal" data-target="#message-modal">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i> Message
                                </a>
                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
        <div class="overlay"></div>
    </div>
    <div class="breadcrumb-block">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item"><a href="favorite-listing.html">{!! $businessinformation->category()->pluck('name')->implode('') !!}</a></li>
                <li class="breadcrumb-item active">{!! $businessinformation->name !!}</li>
            </ol>
            <div class="breadcrumb-call-to-action">
                <div class="post-tags">
                    <a href="javascript:void(0)" class="tags expensive">
                        <i class="fa fa-usd" aria-hidden="true"></i> Expensive
                    </a>
                    <a href="javascript:void(0)" class="tags verified">
                        <i class="fa fa-check" aria-hidden="true"></i> Verified
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="single-post-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 push-lg-3">
                    <article class="single-post">
                        <div class="post-thumb">
                            <img src="images/post/19.jpg" alt="img" class="img-responsive">
                        </div>
                        <p class="post-entry">
                            {!! $listingdetails->description!!}
                        </p>
                        <h5><strong>Promotional Videos</strong></h5>
                        <div class="video-gallary-block">
                            @if($listingdetails->youtubevidone != '')
                            <div class="video-gallary">
                                <a href="{!! $listingdetails->youtubevidone !!}" class="play-btn">
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                </a>
                            </div>
                            @endif
                                @if($listingdetails->youtubevidtwo != '')
                            <div class="video-gallary">
                                <a href="{!! $listingdetails->youtubevidtwo !!}" class="play-btn">
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                </a>
                            </div>
                                @endif
                                @if($listingdetails->youtubevidthree != '')
                            <div class="video-gallary">
                                <a href="{!! $listingdetails->youtubevidthree !!}" class="play-btn">
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                </a>
                            </div>
                                @endif
                                @if($listingdetails->youtubevidfour != '')
                            <div class="video-gallary">
                                <a href="{!! $listingdetails->youtubevidfour !!}" class="play-btn">
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                </a>
                            </div>
                                @endif
                        </div>
                        <div class="faq-section">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1">
                                                How can make reservation without credit card information?
                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="panel1" class="panel-collapse collapse in show">
                                        <div class="panel-body">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel2">
                                                How can make reservation without credit card information?
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </h4>

                                    </div>
                                    <div id="panel2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel3">
                                                How can make reservation without credit card information?
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                        </h4>

                                    </div>
                                    <div id="panel3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- faq-section -->
                    </article>
                    <div class="review-section">
                        <div class="review-title-block">
                            <h4>205 Reviews for <strong>Hotel California</strong></h4>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Short By
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <button class="dropdown-item" type="button">Option One</button>
                                    <button class="dropdown-item" type="button">Option Two</button>
                                    <button class="dropdown-item" type="button">Option Three</button>
                                </div>
                            </div>
                        </div>
                        <div class="comments listing-reviews">
                            <ul>
                                <li>
                                    <div class="avatar-block">
                                        <img src="images/post/author/5.jpg" alt="img" class="img-responsive">
                                        <div class="comment-by">
                                            <h4><a href="javascript:void(0)">Oliver liam</a></h4>
                                            <p><i class="fa fa-star" aria-hidden="true"></i> 90 Reviews</p>
                                        </div>
                                    </div>
                                    <div class="review-content">
                                        <h4>It was an awesome experience</h4>
                                        <div class="meta">
                                                <span class="date">
												<i class="fa fa-calendar" aria-hidden="true"></i>
												25 December 2018
											</span>
                                            <span class="time">
												<i class="fa fa-clock-o" aria-hidden="true"></i>
												10:35pm
											</span>
                                        </div>
                                        <div class="review-ratting">
                                                <span>
												<i class="fa fa-star" aria-hidden="true"></i>
											</span>
                                            <span>
												<i class="fa fa-star" aria-hidden="true"></i>
											</span>
                                            <span>
												<i class="fa fa-star" aria-hidden="true"></i>
											</span>
                                            <span>
												<i class="fa fa-star" aria-hidden="true"></i>
											</span>
                                            <span>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</span>
                                        </div>
                                        <div class="review-entry">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            </p>
                                        </div>
                                        <div class="review-images-block">
                                            <a href="images/post/review/large/1.jpg" class="review-images">
                                                <img src="images/post/review/1.jpg" alt="img" class="img-responsive">
                                            </a>
                                            <a href="images/post/review/large/2.jpg" class="review-images">
                                                <img src="images/post/review/2.jpg" alt="img" class="img-responsive">
                                            </a>
                                            <a href="images/post/review/large/3.jpg" class="review-images">
                                                <img src="images/post/review/3.jpg" alt="img" class="img-responsive">
                                            </a>
                                            <a href="images/post/review/large/4.jpg" class="review-images">
                                                <img src="images/post/review/4.jpg" alt="img" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="rate-review-block">
                                            <h5>Was this review helpful to you?</h5>
                                            <div class="btn-group">
                                                <a href="javascript:void(0)" class="rate-btn toggole-contnet">
                                                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Yes
                                                </a>
                                                <a href="javascript:void(0)" class="rate-btn toggole-contnet">
                                                    <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> No
                                                </a>
                                            </div>
                                            <a href="javascript:void(0)" class="report-link">
                                                Report Abuse
                                            </a>
                                        </div>
                                        <a href="javascript:void(0)" class="replay-btn">
                                            <i class="fa fa-reply" aria-hidden="true"></i> Replay
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="avatar-block">
                                        <img src="images/post/author/5.jpg" alt="img" class="img-responsive">
                                        <div class="comment-by">
                                            <h4><a href="javascript:void(0)">Oliver liam</a></h4>
                                            <p><i class="fa fa-star" aria-hidden="true"></i> 90 Reviews</p>
                                        </div>
                                    </div>
                                    <div class="review-content">
                                        <h4>It was an awesome experience</h4>
                                        <div class="meta">
                                                <span class="date">
												<i class="fa fa-calendar" aria-hidden="true"></i>
												25 December 2018
											</span>
                                            <span class="time">
												<i class="fa fa-clock-o" aria-hidden="true"></i>
												10:35pm
											</span>
                                        </div>
                                        <div class="review-ratting">
                                                <span>
												<i class="fa fa-star" aria-hidden="true"></i>
											</span>
                                            <span>
												<i class="fa fa-star" aria-hidden="true"></i>
											</span>
                                            <span>
												<i class="fa fa-star" aria-hidden="true"></i>
											</span>
                                            <span>
												<i class="fa fa-star" aria-hidden="true"></i>
											</span>
                                            <span>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</span>
                                        </div>
                                        <div class="review-entry">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                            </p>
                                        </div>
                                        <div class="review-images-block">
                                            <a href="images/post/review/large/5.jpg" class="review-images">
                                                <img src="images/post/review/5.jpg" alt="img" class="img-responsive">
                                            </a>
                                            <a href="images/post/review/large/6.jpg" class="review-images">
                                                <img src="images/post/review/6.jpg" alt="img" class="img-responsive">
                                            </a>
                                            <a href="images/post/review/large/7.jpg" class="review-images">
                                                <img src="images/post/review/7.jpg" alt="img" class="img-responsive">
                                            </a>
                                            <a href="images/post/review/large/8.jpg" class="review-images">
                                                <img src="images/post/review/8.jpg" alt="img" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="rate-review-block">
                                            <h5>Was this review helpful to you?</h5>
                                            <div class="btn-group">
                                                <a href="javascript:void(0)" class="rate-btn toggole-contnet">
                                                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Yes
                                                </a>
                                                <a href="javascript:void(0)" class="rate-btn toggole-contnet">
                                                    <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> No
                                                </a>
                                            </div>
                                            <a href="javascript:void(0)" class="report-link">
                                                Report Abuse
                                            </a>
                                        </div>
                                        <a href="javascript:void(0)" class="replay-btn">
                                            <i class="fa fa-reply" aria-hidden="true"></i> Replay
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- listing-reviews -->
                        <div class="write-review-section">
                            <div class="write-review-title">
                                <h4>Rate & Write a Review</h4>
                            </div>
                            <div class="write-review-inner">
                                <h4>Your review will be posted publicly on the web.</h4>
                                <form class="form-common">
                                    <div class="form-group ratting-area">
                                        <div class='rating-stars text-center'>
                                            <ul id='stars'>
                                                <li class='star' title='Poor' data-value='1'>
                                                    <i class='fa fa-star fa-fw'></i>
                                                </li>
                                                <li class='star' title='Fair' data-value='2'>
                                                    <i class='fa fa-star fa-fw'></i>
                                                </li>
                                                <li class='star' title='Good' data-value='3'>
                                                    <i class='fa fa-star fa-fw'></i>
                                                </li>
                                                <li class='star' title='Excellent' data-value='4'>
                                                    <i class='fa fa-star fa-fw'></i>
                                                </li>
                                                <li class='star' title='WOW!!!' data-value='5'>
                                                    <i class='fa fa-star fa-fw'></i>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="userName">Name</label>
                                        <input type="text" class="form-control" id="userName" placeholder="Enter your name here">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" id="email" placeholder="Enter your email">
                                    </div>
                                    <div class="form-group">
                                        <label for="review-input-title">Title</label>
                                        <input type="text" class="form-control" id="review-input-title" placeholder="Write the title">
                                    </div>
                                    <div class="form-group textarea-form-group">
                                        <label for="review-input-entry">Review</label>
                                        <textarea rows="8" cols="50" id="review-input-entry" class="form-control" placeholder="Write your review here"></textarea>
                                    </div>
                                    <div class="filetype-form-group">
                                        <label class="custom-file" for="Upload"></label>
                                        <input id="Upload" type="file" multiple="multiple" name="_photos" accept="image/*" style="visibility: hidden">
                                    </div>
                                    <button type="submit" class="listing-btn-large">Signup & Post Review</button>
                                </form>
                            </div>
                        </div>
                        <!-- review-write-section -->
                    </div>
                </div>
                <div class="col-lg-3 pull-lg-9">
                    <div class="sidebar">
                        <div id="map_widget" class="widget map-widget"></div>
                        <div class="widget contact-widget">
                            <p class="address">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span> {!! $businessinformation->address !!}</span>
                            </p>
                            <p>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span>
									{!! $businessinformation->phone !!}
								</span>
                            </p>
                            <p>
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <span>
									{!! $businessinformation->email !!}
								</span>
                            </p>
                            <p>
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                <span>
									{!! $businessinformation->website !!}
								</span>
                            </p>
                            <ul class="social">
                                @if($listingdetails->facebookurl !='')
                                <li>
                                    <a href="{!! $listingdetails->facebookurl !!}">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                @endif
                                @if($listingdetails->twitterurl !='')
                                <li>
                                    <a href="{!! $listingdetails->twitterurl !!}">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                                    @endif
                                    @if($listingdetails->youtubeurl !='')
                                <li>
                                    <a href="{!! $listingdetails->youtubeurl !!}">
                                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                    </a>
                                </li>
                                    @endif
                                    @if($listingdetails->instagramurl !='')
                                    <li>
                                    <a href="{!! $listingdetails->instagramurl !!}">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                                    @endif
                                    @if($listingdetails->googleplusurl !='')
                                <li>
                                    <a href="{!! $listingdetails->googleplusurl !!}">
                                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                                    </a>
                                </li>
                                    @endif
                                    @if($listingdetails->linkedinurl !='')
                                <li>
                                    <a href="{!! $listingdetails->linkedinurl !!}">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                                    @endif
                            </ul>
                        </div>
                        <!-- widget -->
                        <div class="widget businesshours-widget">
                            <div class="widget-title">
                                <h5>
                                    <i class="fa fa-clock-o" aria-hidden="true"></i> Opening Hours
                                </h5>
                                <h5 class="current-schedule">Open</h5>
                            </div>
                            <div class="widget-body">
                                <div class="businesshours">
                                    <div class="hrs-row">
                                            <span class="day">
											Monday
										</span>
                                        <span class="hours">
											10am - 10pm
										</span>
                                    </div>
                                    <div class="hrs-row">
                                            <span class="day">
											Tuesday
										</span>
                                        <span class="hours">
											10am - 10pm
										</span>
                                    </div>
                                    <div class="hrs-row">
                                            <span class="day">
											Wednesday
										</span>
                                        <span class="hours">
											10am - 10pm
										</span>
                                    </div>
                                    <div class="hrs-row">
                                            <span class="day">
											Thursday
										</span>
                                        <span class="hours">
											10am - 10pm
										</span>
                                    </div>
                                    <div class="hrs-row">
                                            <span class="day">
											Friday
										</span>
                                        <span class="hours">
											10am - 10pm
										</span>
                                    </div>
                                    <div class="hrs-row">
                                            <span class="day">
											Saturday
										</span>
                                        <span class="hours">
											10am - 10pm
										</span>
                                    </div>
                                    <div class="hrs-row">
                                            <span class="day">
											Sunday
										</span>
                                        <span class="hours">
											10am - 10pm
										</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- widget -->
                        <div class="widget othersinfo-widget">
                            <div class="widget-title">
                                <h5>
                                    <i class="fa fa-info-circle" aria-hidden="true"></i> Others Info
                                </h5>
                            </div>
                            <div class="widget-body">
                                <div class="othersinfo-row">
                                        <span class="left-info">
										Price Range
									</span>
                                    <span class="right-info">
										$10 - $200
									</span>
                                </div>
                                <div class="othersinfo-row">
                                        <span class="left-info">
										Full Menu
									</span>
                                    <span class="right-info">
										<a href="javscript:void(0)">
											Download
										</a>
									</span>
                                </div>
                                <div class="othersinfo-row">
                                        <span class="left-info">
										Company Profile
									</span>
                                    <span class="right-info">
										<a href="javscript:void(0)">
											Download
										</a>
									</span>
                                </div>
                            </div>
                        </div>
                        <!-- widget -->
                        <div class="widget ad-widget">
                            <a href="javscript:void(0)">
                                <img src="images/misc/ad.jpg" alt="img" class="img-responsive">
                            </a>
                        </div>
                        <!-- widget -->
                        <div class="widget contact-form-wdiget">
                            <div class="widget-title">
                                <h5>
                                    <i class="fa fa-info-circle" aria-hidden="true"></i> ListingGEO Contact
                                </h5>
                            </div>
                            <div class="widget-body">
                                <form class="form-common">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Phone">
                                    </div>
                                    <div class="form-group textarea-form-group">
                                        <textarea rows="5" cols="5" class="form-control" placeholder="Message"></textarea>
                                    </div>
                                    <button type="submit" class="listing-btn-large">Send</button>
                                </form>
                            </div>
                        </div>
                        <!-- widget -->
                        <div class="widget listingpost-widget">
                            <div class="widget-title">
                                <h5>
                                    <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i> Related Listings
                                </h5>
                            </div>
                            <article class="popular-listing-post popular-post-two">
                                <div class="post-thumb">
                                    <img src="images/post/17.jpg" alt="img" class="img-responsive">
                                    <div class="listing-info">
                                        <p class="meta-tag"><a href="javascript:void(0)">Hotel & Restaurent</a></p>
                                        <h4><a href="javascript:void(0)">Hotel California</a></h4>
                                        <p class="address-info"><a href="javascript:void(0)"><i class="fa fa-map-marker" aria-hidden="true"></i> 178 Zavee Strre, California</a></p>
                                    </div>
                                    <div class="price-tag">
                                        <p><i class="fa fa-usd" aria-hidden="true"></i>40 - <i class="fa fa-usd" aria-hidden="true"></i>200</p>
                                    </div>
                                    <div class="rating-area">
                                        <ul>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        </ul>
                                    </div>
                                    <div class="overlay"></div>
                                </div>
                            </article>
                            <article class="popular-listing-post popular-post-two">
                                <div class="post-thumb">
                                    <img src="images/post/16.jpg" alt="img" class="img-responsive">
                                    <div class="listing-info">
                                        <p class="meta-tag"><a href="javascript:void(0)">Hotel & Restaurent</a></p>
                                        <h4><a href="javascript:void(0)">Hotel California</a></h4>
                                        <p class="address-info"><a href="javascript:void(0)"><i class="fa fa-map-marker" aria-hidden="true"></i> 178 Zavee Strre, California</a></p>
                                    </div>
                                    <div class="price-tag">
                                        <p><i class="fa fa-usd" aria-hidden="true"></i>40 - <i class="fa fa-usd" aria-hidden="true"></i>200</p>
                                    </div>
                                    <div class="rating-area">
                                        <ul>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        </ul>
                                    </div>
                                    <div class="overlay"></div>
                                </div>
                            </article>
                            <article class="popular-listing-post popular-post-two">
                                <div class="post-thumb">
                                    <img src="images/post/18.jpg" alt="img" class="img-responsive">
                                    <div class="listing-info">
                                        <p class="meta-tag"><a href="javascript:void(0)">Hotel & Restaurent</a></p>
                                        <h4><a href="javascript:void(0)">Hotel California</a></h4>
                                        <p class="address-info"><a href="javascript:void(0)"><i class="fa fa-map-marker" aria-hidden="true"></i> 178 Zavee Strre, California</a></p>
                                    </div>
                                    <div class="price-tag">
                                        <p><i class="fa fa-usd" aria-hidden="true"></i>40 - <i class="fa fa-usd" aria-hidden="true"></i>200</p>
                                    </div>
                                    <div class="rating-area">
                                        <ul>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        </ul>
                                    </div>
                                    <div class="overlay"></div>
                                </div>
                            </article>
                        </div>
                        <!-- widget -->
                        <div class="widget author-widget">
                            <div class="widget-title">
                                <h5>
                                    <i class="fa fa-info-circle" aria-hidden="true"></i> Listed By
                                </h5>
                            </div>
                            <div class="author-widget-body">
                                <div class="thumb">
                                    <img src="images/post/author/7.jpg" alt="img" class="img-responsive">
                                </div>
                                <div class="info">
                                    <h4>Anderson Mark</h4>
                                    <span>Member Since December 15</span>
                                </div>
                                <div class="btn-group">
                                    <a href="javascript:void(0)" class="success-btn">
                                        View Profile
                                    </a>
                                    <a href="javascript:void(0)" class="danger-btn">
                                        Contact
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- widget -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- single-post-section -->
    <div class="call-to-action-section">
        <div class="container">
            <h2>
                Start today to get more exposure and <br> grow your business
            </h2>
            <p>
                There are many consumers & business owners find this directory is helpful.
            </p>
            <div class="btn-group">
                <a href="add-listing.html" class="icon-btn adining-listing-btn">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add Listing
                </a>
                <a href="single-listing.html" class="icon-btn explore-listing-btn">
                    <i class="fa fa-search" aria-hidden="true"></i> Explore Listing
                </a>
            </div>
        </div>
    </div>
    <div class="listing-single-modal-content-section">
        <div class="modal fade" id="claim-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Inform Us</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-common">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Business name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Street address">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Postal Code">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Main business phone">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Category">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Website">
                            </div>
                            <div class="form-group">
                                <label class="sca">I deliver goods and services to my customers at their location —<a href="#">Important information</a></label>
                                <label class="custom-control custom-radio">
                                    <input name="cat" type="radio" class="custom-control-input" value="1">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Yes</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input name="cat" type="radio" class="custom-control-input" value="0">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">No</span>
                                </label>
                            </div>
                            <button type="submit" class="listing-btn-large">Continue</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="message-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel2">Text Us</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-common">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group textarea-form-group">
                                <textarea rows="5" cols="5" class="form-control" placeholder="Message"></textarea>
                            </div>
                            <button type="submit" class="listing-btn-large">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- listing-single-modal-content section -->
    <!-- call-to-action section-->
@endsection