@extends('layouts.user')
@section('title','ListingGEO - Login')
@section('main-content')
    <div class="subheader">
        <h2>User Log in</h2>
        <div class="overlay"></div>
    </div>
    <div class="breadcrumb-block">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">User Log in</li>
            </ol>
            <div class="breadcrumb-call-to-action">
                <p><i class="fa fa-phone" aria-hidden="true"></i> +1-0000-000-000</p>
                <a href="javascript:void(0)" class="contact-btn">
                    Contact Us
                </a>
            </div>
        </div>
    </div>
    <div class="user-form-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-12">
                    <div class="registration-form-block">
                        <div class="registration-form-title">
                            <h4>Log in</h4>
                        </div>
                        <div class="form-block">
                            <form class="form-common" action="{{ route('login') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="email">Email Address *</label>
                                    <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="Enter your email" name="email" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Password *</label>
                                    <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Enter your Password" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group row form-check-row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="form-check">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" {{ old('remember') ? 'checked' : '' }} name="remember">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Remember Me</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="forgot-link-block">
                                                <a href="{{ route('password.request') }}" class="forgot-link">Forget Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-btn-block">
                                        <button type="submit" class="form-btn">Sign in</button>
                                    </div>
                                </div>
                            </form>
                            <div class="signin-others-option-block">
                                <h5>You don't have an account? <a href="{!! route('register') !!}">Register here</a></h5>
                            </div>
                        </div>
                    </div>
                    <!-- panel -->
                </div>
            </div>
        </div>
    </div>
    <div class="call-to-action-section">
        <div class="container">
            <h2>
                Start today to get more exposure and <br> grow your business
            </h2>
            <p>
                There are many consumers & business owners find this directory is helpful.
            </p>
            <div class="btn-group">
                <a href="add-listing.html" class="icon-btn adining-listing-btn">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add Listing
                </a>
                <a href="single-listing.html" class="icon-btn explore-listing-btn">
                    <i class="fa fa-search" aria-hidden="true"></i> Explore Listing
                </a>
            </div>
        </div>
    </div>
    <!-- call-to-action section-->
@endsection
