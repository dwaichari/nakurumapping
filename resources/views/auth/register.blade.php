
@extends('layouts.user')
@section('title','ListingGEO - Register')
@section('main-content')
    <div class="subheader">
        <h2>User Registration</h2>
        <div class="overlay"></div>
    </div>
    <div class="breadcrumb-block">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">User Registration</li>
            </ol>
            <div class="breadcrumb-call-to-action">
                <p><i class="fa fa-phone" aria-hidden="true"></i> +1-0000-000-000</p>
                <a href="javascript:void(0)" class="contact-btn">
                    Contact Us
                </a>
            </div>
        </div>
    </div>
    <div class="user-form-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-12">
                    <div class="registration-form-block">
                        <div class="registration-form-title">
                            <h4>Create Account</h4>
                        </div>
                        <div class="form-block">
                            <form class="form-common" action="{{ route('register') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="firstname">First Name *</label>
                                    <input type="text" class="form-control {{ $errors->has('firstname') ? ' is-invalid' : '' }}" id="firstname" placeholder="Enter your First Name" name="firstname" required>
                                    @if ($errors->has('firstname'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="lastname">Last Name *</label>
                                    <input type="text" class="form-control {{ $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname" placeholder="Enter your Last Name" name="lastname" required>
                                    @if ($errors->has('lastname'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="residence">Residence *</label>
                                    <input type="text" class="form-control {{ $errors->has('residence') ? ' is-invalid' : '' }}" id="residence" placeholder="Enter your Residence" name="residence" required>
                                    @if ($errors->has('residence'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('residence') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="residence">Contact *</label>
                                    <input type="text" class="form-control {{ $errors->has('contact') ? ' is-invalid' : '' }}" id="contact" placeholder="Enter your Phone Number" name="contact" required>
                                    @if ($errors->has('contact'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">Email Address *</label>
                                    <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="Enter your email" name="email" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Password *</label>
                                    <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Enter your Password" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password-confirm">Confirm Password *</label>
                                    <input type="password" class="form-control" id="password-confirm" placeholder="Confirm Password" name="password_confirmation" required>
                                </div>
                                <div class="form-group">
                                    <div class="form-btn-block">
                                        <button type="submit" class="form-btn">Submit</button>
                                    </div>
                                </div>
                            </form>
                            <div class="signin-others-option-block">
                                <h5>You already have an account? | <a href="{!! route('login') !!}">Log in here</a></h5>
                            </div>
                        </div>
                    </div>
                    <!-- panel -->
                </div>
            </div>
        </div>
    </div>
    <div class="call-to-action-section">
        <div class="container">
            <h2>
                Start today to get more exposure and <br> grow your business
            </h2>
            <p>
                There are many consumers & business owners find this directory is helpful.
            </p>
            <div class="btn-group">
                <a href="add-listing.html" class="icon-btn adining-listing-btn">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add Listing
                </a>
                <a href="single-listing.html" class="icon-btn explore-listing-btn">
                    <i class="fa fa-search" aria-hidden="true"></i> Explore Listing
                </a>
            </div>
        </div>
    </div>
    <!-- call-to-action section-->
@endsection
