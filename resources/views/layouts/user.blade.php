
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from eorrangeshop.com/html/listingGeo/home-one.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Mar 2018 10:04:45 GMT -->
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="location listing creative">
    <meta name="author" content="CodePassenger">

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" type='text/css'>

    <link rel="stylesheet" href="{!! asset('/frontend/assets/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/frontend/assets/css/themify-icons.css') !!}">
    <link rel="stylesheet" href="{!! asset('/frontend/assets/css/simple-line-icons.css') !!}">
    <link rel="stylesheet" href="{!! asset('/frontend/assets/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/frontend/assets/css/jquery-ui.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/frontend/assets/css/magnific-popup.css') !!}">

    <!-- BX Slider CSS -->
    <link rel="stylesheet" href="{!! asset('/frontend/assets/css/jquery.bxslider.css') !!}">

    <link rel="stylesheet" href="{!! asset('/frontend/css/style.css')!!}">

    <link rel="stylesheet" href="{!! asset('/frontend/css/responsive.css') !!}">
</head>

<body>
<div class="main-wrap">
    <!-- Main Navigation -->
   @include('layouts.navbar')
    <!-- main nav section -->
    @section('main-content')
        @show
    <footer>
        <div class="footer-top-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="footer-widget">
                            <div class="footer-logo-block">
                                <a href="javascript:void(0)">
                                    <img src="{!! asset('frontend/images/logo.png') !!}" alt="img" class="img-responsive">
                                </a>
                            </div>
                            <p class="address">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> Suite # 25/B, Green Street California, CA78542
                            </p>
                            <p><i class="fa fa-phone" aria-hidden="true"></i> +1-0000-000-000</p>
                            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> info@example.com</p>
                            <div class="footer-social-block">
                                    <span>
										Folow us:
									</span>
                                <ul class="social">
                                    <li>
                                        <a href="javascript:void(0)">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- footer-social-block -->
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="footer-widget">
                            <h4 class="footer-widget-title">Useful Links</h4>
                            <ul class="footer-content-list">
                                <li>
                                    <a href="javascript:void(0)">
                                        About ListingGEO
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        How it Works
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        Exclusive Listings
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        Popular Locations
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        Contact us
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="footer-widget">
                            <h4 class="footer-widget-title">Listing Account</h4>
                            <ul class="footer-content-list">
                                <li>
                                    <a href="javascript:void(0)">
                                        User Log in
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        User Registration
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        Add Listing
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        Favorite Lisitings
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        Pricing Plans
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="footer-widget">
                            <h4 class="footer-widget-title">Latest Listings</h4>
                            <article class="latest-post">
                                <div class="post-thumb">
                                    <a href="javascript:void(0)">
                                        <img src="{!! asset('frontend/images/post/7.jpg') !!}" alt="img" class="img-responsive">
                                    </a>
                                </div>
                                <div class="post-wrapper">
                                    <h6 class="title">
                                        <a href="javascript:void(0)">
                                            Grand Park Hotel
                                        </a>
                                    </h6>
                                    <p class="post-entry">
                                        175 Church Road, City Tower, California, CA785423
                                    </p>
                                    <div class="post-meta">
                                        <a href="javascript:void(0)" class="post-tag">
                                            Hotel & Resort
                                        </a>
                                    </div>
                                </div>
                            </article>
                            <article class="latest-post">
                                <div class="post-thumb">
                                    <a href="javascript:void(0)">
                                        <img src="{!! asset('frontend/images/post/7.jpg') !!}" alt="img" class="img-responsive">
                                    </a>
                                </div>
                                <div class="post-wrapper">
                                    <h6 class="title">
                                        <a href="javascript:void(0)">
                                            Grand Park Hotel
                                        </a>
                                    </h6>
                                    <p class="post-entry">
                                        175 Church Road, City Tower, California, CA785423
                                    </p>
                                    <div class="post-meta">
                                        <a href="javascript:void(0)" class="post-tag">
                                            Hotel & Resort
                                        </a>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 push-md-6">
                        <ul class="footer-nav">
                            <li>
                                <a href="javascript:void(0)">
                                    Legal
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    Privacy Policy
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    Terms of Use
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 pull-md-6">
                        <p class="copyright-text">Copyright 2018, <a href="javascript:void(0)">ListingGEO</a>. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="{!! asset('frontend/assets/js/jquery-3.2.1.min.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/tether.min.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/jquery.bxslider.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/jquery.magnific-popup.min.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/jquery.ajaxchimp.min.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/jquery-ui.min.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/jquery.waypoints.min.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/jquery.counterup.min.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/lobipanel.min.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/jquery.accordion.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/jquery.slimscroll.min.js') !!}"></script>
<!-- Tinymce-JS -->
<script src="{!! asset('frontend/assets/js/tinymce/tinymce.min.js') !!}"></script>
<!-- Google-map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmiJjq5DIg_K9fv6RE72OY__p9jz0YTMI"></script>
<script src="{!! asset('frontend/js/custom.js') !!}"></script>
<script src="{!! asset('frontend/js/mapdata.json') !!}"></script>
<script src="{!! asset('frontend/js/map.js') !!}"></script>

<script src="{!! asset('frontend/js/user.js') !!}"></script>
<script src="{!! asset('frontend/assets/js/gmap3.min.js') !!}"></script>
</body>

<!-- Mirrored from eorrangeshop.com/html/listingGeo/home-one.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Mar 2018 10:06:10 GMT -->
</html>