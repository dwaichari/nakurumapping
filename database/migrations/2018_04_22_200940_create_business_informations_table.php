<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tagline')->nullable();
            $table->string('email');
            $table->string('phone');
            $table->string('website')->nullable();
            $table->integer('subcounty_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('ward_id')->unsigned();
            $table->string('address');
            $table->double('latitude');
            $table->double('longitude');
            $table->integer('category_id');
            $table->integer('subcategory_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_informations');
    }
}
