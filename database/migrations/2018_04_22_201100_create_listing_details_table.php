<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('businessinformation_id');
            $table->text('description')->nullable();
            $table->string('facebookurl')->nullable();
            $table->string('twitterurl')->nullable();
            $table->string('instagramurl')->nullable();
            $table->string('googleplusurl')->nullable();
            $table->string('youtubeurl')->nullable();
            $table->string('linkedinurl')->nullable();
            $table->string('youtubevidone')->nullable();
            $table->string('youtubevidtwo')->nullable();
            $table->string('youtubevidthree')->nullable();
            $table->string('youtubevidfour')->nullable();
            $table->text('keywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_details');
    }
}
