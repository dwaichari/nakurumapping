<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $addCategory = new \App\Category();
        $addCategory->name = 'Creative Cultural Indusries';
        $addCategory->save();

        $addCategory = new \App\Category();
        $addCategory->name = 'County Cultural Organization';
        $addCategory->save();

        $addCategory = new \App\Category();
        $addCategory->name = 'Spaces and Facilities';
        $addCategory->save();

        $addCategory = new \App\Category();
        $addCategory->name = 'Festivals and Events';
        $addCategory->save();

        $addCategory = new \App\Category();
        $addCategory->name = 'Cultural Heritage';
        $addCategory->save();

        $addCategory = new \App\Category();
        $addCategory->name = 'Natural Heritage';
        $addCategory->save();
    }
}
