<?php

use Illuminate\Database\Seeder;

class SubCountiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Bahati';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Gilgil';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Kuresoi North';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Kuresoi South';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Molo';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Naivasha';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Nakuru East';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Nakuru West';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Njoro';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Rongai';
        $subcounty->save();

        $subcounty = new \App\SubCounty();
        $subcounty->name = 'Subukia';
        $subcounty->save();
    }
}
