<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessInformation extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(SubCategory::class);
    }

    public function subcounty()
    {
        return $this->belongsTo(SubCounty::class);
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class);
    }

    public function listingdetails()
    {
        return $this->hasOne(ListingDetails::class,'businessinformation_id');
    }
    public function companylogo()
    {
        return $this->hasOne(CompanyLogo::class,'businessinformation_id');
    }
    public function coverimage()
    {
        return $this->hasOne(CoverImage::class,'businessinformation_id');
    }
    public function slideshows()
    {
        return $this->hasMany(SlideShow::class,'businessinformation_id');
    }
    public function galleries()
    {
        return $this->hasMany(Gallery::class,'businessinformation_id');
    }
    public function companyprofile()
    {
        return $this->hasOne(CompanyProfile::class,'businessinformation_id');
    }
    public function companybronchure()
    {
        return $this->hasOne(CompanyBronchure::class,'businessinformation_id');
    }
    public function menu()
    {
        return $this->hasOne(Menu::class,'businessinformation_id');
    }
    public function other()
    {
        return $this->hasOne(Other::class,'businessinformation_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function businessdays()
    {
        return $this->hasMany(BusinessDays::class,'businessinformation_id');
    }

    public function faqs()
    {
        return $this->hasMany(Faq::class,'businessinformation_id');
    }
}
