<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyLogo extends Model
{
    public function businessinformation()
    {
        return $this->belongsTo(BusinessInformation::class);
    }
}
