<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    public function businessinformations()
    {
        return $this->hasMany(BusinessInformation::class,'ward_id');
    }
}
