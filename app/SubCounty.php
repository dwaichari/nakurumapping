<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCounty extends Model
{
    public function wards()
    {
        return $this->hasMany(Ward::class,'subcounty_id');
    }
    public function businessinformations()
    {
        return $this->hasMany(BusinessInformation::class,'subcounty_id');
    }
}
