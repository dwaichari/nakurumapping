<?php

namespace App\Http\Controllers;

use App\BusinessInformation;
use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('user.home',compact(['categories']));
    }
    public function listingsMap($id)
    {
        $category = Category::find($id);
        $listings = Category::find($id)->businessinformations;
        return view('user.listingsmap',compact(['listings','category']));
    }

    public function listingDetails($id)
    {
        $businessinformation = BusinessInformation::find($id);
        $listingdetails = BusinessInformation::find($id)->listingdetails;
        return view('user.listingdetails',compact(['businessinformation','listingdetails']));
    }

}
