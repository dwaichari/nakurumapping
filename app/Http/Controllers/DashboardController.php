<?php

namespace App\Http\Controllers;

use App\BusinessDays;
use App\BusinessInformation;
use App\Category;
use App\CompanyBronchure;
use App\CompanyLogo;
use App\CompanyProfile;
use App\CoverImage;
use App\Faq;
use App\Gallery;
use App\ListingDetails;
use App\Menu;
use App\Other;
use App\SlideShow;
use App\SubCategory;
use App\SubCounty;
use App\Ward;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        return view('dashboard.dashboard');
    }
    public function allListing()
    {
        $businessinformations = auth()->user()->businessinformations;
        return view('dashboard.listings.alllistings',compact(['businessinformations']));
    }
    public function addListing()
    {
        $faqs = Faq::all();
        $businessdays = BusinessDays::all();
        $categories = Category::all();
        $subcounties = SubCounty::all();
        return view('dashboard.listings.add',compact(['categories','subcounties','businessdays','faqs']));
    }
    public function storeListing(Request $request)
    {
        $addbusiness = new BusinessInformation();
        $addbusiness->name = $request->name;
        $addbusiness->tagline = $request->tagline;
        $addbusiness->user_id = auth()->user()->id;
        $addbusiness->email = $request->email;
        $addbusiness->phone = $request->phone;
        $addbusiness->website = $request->website;
        $addbusiness->subcounty_id = $request->subcounty_id;
        $addbusiness->ward_id = $request->ward_id;
        $addbusiness->address = $request->address;
        $addbusiness->latitude = $request->latitude;
        $addbusiness->longitude = $request->longitude;
        $addbusiness->category_id = $request->category_id;
        $addbusiness->subcategory_id = $request->subcategory_id;
        $addbusiness->save();

        $listingdetails = new ListingDetails();
        $listingdetails->businessinformation_id = $addbusiness->id;
        $listingdetails->description = $request->description;
        $listingdetails->facebookurl = $request->facebookurl;
        $listingdetails->twitterurl = $request->twitterurl;
        $listingdetails->instagramurl = $request->instagramurl;
        $listingdetails->googleplusurl = $request->googleplusurl;
        $listingdetails->youtubeurl = $request->youtubeurl;
        $listingdetails->linkedinurl = $request->linkedinurl;
        $listingdetails->youtubevidone = $request->youtubevidone;
        $listingdetails->youtubevidtwo = $request->youtubevidtwo;
        $listingdetails->youtubevidthree = $request->youtubevidthree;
        $listingdetails->youtubevidfour = $request->youtubevidfour;
        $listingdetails->keywords = $request->listing_kewords;
        $addbusiness->listingdetails()->save($listingdetails);

        // Handle File Logo Upload
        if($request->hasFile('listing_company_logo')){
            // Get filename with the extension
            $filenameWithExt = $request->file('listing_company_logo')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('listing_company_logo')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('listing_company_logo')->storeAs('public/Company Logos/'.$request->name, $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        // Create Logo
        $addLogo = new CompanyLogo();
        $addLogo->businessinformation_id = $addbusiness->id;
        $addLogo->name = $fileNameToStore;
        $addbusiness->companylogo()->save($addLogo);

        //to handle cover image upload
        if($request->hasFile('listing_company_cover_photo')){
            // Get filename with the extension
            $filenameWithExt = $request->file('listing_company_cover_photo')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('listing_company_cover_photo')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('listing_company_cover_photo')->storeAs('public/Cover Images/'.$request->name, $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        // Create Cover Image
        $addCoverImage = new CoverImage();
        $addCoverImage->businessinformation_id = $addbusiness->id;
        $addCoverImage->name = $fileNameToStore;
        $addbusiness->coverimage()->save($addCoverImage);

        //to handle slideshow upload
        if($request->hasFile('listing_company_slide_photo')){
            $slideshows = $request->file('listing_company_slide_photo');
            foreach ($slideshows as $slidephoto)
            {
                // Get filename with the extension
                $filenameWithExt = $slidephoto->getClientOriginalName();
                // Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                // Get just ext
                $extension = $slidephoto->getClientOriginalExtension();
                // Filename to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;
                // Upload Image
                $path = $slidephoto->storeAs('public/Slide Shows/'.$request->name, $fileNameToStore);

                // Create Slide Shows
                 $addSlideShow = new SlideShow();
                 $addSlideShow->businessinformation_id = $addbusiness->id;
                 $addSlideShow->name = $fileNameToStore;
                 $addbusiness->slideshows()->save($addSlideShow);
            }

        } else {
            // Create SlideShow
            $addSlideShow = new SlideShow();
            $addSlideShow->businessinformation_id = $addbusiness->id;
            $addSlideShow->name = 'noimage.jpg';
            $addbusiness->slideshows()->save($addSlideShow);
        }

        //to handle gallery upload
        if($request->hasFile('listing_company_gallery_photo')){
            $galleries = $request->file('listing_company_gallery_photo');
            foreach ($galleries as $gallery)
            {
                // Get filename with the extension
                $filenameWithExt = $gallery->getClientOriginalName();
                // Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                // Get just ext
                $extension = $gallery->getClientOriginalExtension();
                // Filename to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;
                // Upload Image
                $path = $gallery->storeAs('public/Gallery/'.$request->name, $fileNameToStore);

                // Create Gallery
                $addGallery = new Gallery();
                $addGallery->businessinformation_id = $addbusiness->id;
                $addGallery->name = $fileNameToStore;
                $addbusiness->galleries()->save($addGallery);
            }

        } else {
            // Create Gallery
            $addGallery = new Gallery();
            $addGallery->businessinformation_id = $addbusiness->id;
            $addGallery->name = 'noimage.jpg';
            $addbusiness->galleries()->save($addGallery);
        }

        //to handle company Profile
        if($request->hasFile('listing_company_profile')){
            // Get filename with the extension
            $filenameWithExt = $request->file('listing_company_profile')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('listing_company_profile')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('listing_company_profile')->storeAs('public/Company Profiles/'.$request->name, $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        // Create Company Profile
        $addProfile = new CompanyProfile();
        $addProfile->businessinformation_id = $addbusiness->id;
        $addProfile->name = $fileNameToStore;
        $addbusiness->companyprofile()->save($addProfile);

        //to handle company Bronchure
        if($request->hasFile('listing_company_brochure')){
            // Get filename with the extension
            $filenameWithExt = $request->file('listing_company_brochure')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('listing_company_brochure')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('listing_company_profile')->storeAs('public/Company Bronchures/'.$request->name, $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        // Create Company Bronchure
        $addBronchure = new CompanyBronchure();
        $addBronchure->businessinformation_id = $addbusiness->id;
        $addBronchure->name = $fileNameToStore;
        $addbusiness->companybronchure()->save($addBronchure);

        //to handle company Menu
        if($request->hasFile('listing_company_menu')){
            // Get filename with the extension
            $filenameWithExt = $request->file('listing_company_menu')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('listing_company_menu')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('listing_company_menu')->storeAs('public/Company Menu/'.$request->name, $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        // Create Company Menu
        $addMenu = new Menu();
        $addMenu->businessinformation_id = $addbusiness->id;
        $addMenu->name = $fileNameToStore;
        $addbusiness->menu()->save($addMenu);

        //to handle other documents
        if($request->hasFile('listing_company_document')){
            // Get filename with the extension
            $filenameWithExt = $request->file('listing_company_document')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('listing_company_document')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('listing_company_document')->storeAs('public/Other Documents/'.$request->name, $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        // Create Other Documents
        $addOther = new Other();
        $addOther->businessinformation_id = $addbusiness->id;
        $addOther->name = $fileNameToStore;
        $addbusiness->other()->save($addOther);

        return redirect(route('dashboard.showListing',$addbusiness->id));
    }

    public function showListing($id)
    {
        $businessdays = BusinessInformation::find($id)->businessdays;
        $listingdetails = BusinessInformation::find($id)->listingdetails;
        $faqs = BusinessInformation::find($id)->faqs;
        $businessinformation = BusinessInformation::find($id);
        return view('dashboard.listings.show',compact(['businessinformation','businessdays','faqs','listingdetails']));
    }

    public function editListing($id)
    {
        $subcounties = SubCounty::all();
        $categories = Category::all();
        $businessdays = BusinessInformation::find($id)->businessdays;
        $listingdetails = BusinessInformation::find($id)->listingdetails;
        $faqs = BusinessInformation::find($id)->faqs;
        $businessinformation = BusinessInformation::find($id);
        return view('dashboard.listings.edit',compact(['businessinformation','businessdays','faqs','listingdetails','categories','subcounties']));

    }

    public function getWards(Request $request)
    {
        if ($request->ajax()) {
            $wards = SubCounty::find($request->id)->wards;
            return response()->json($wards);
        }
    }
    public function getWardCoordinates(Request $request)
    {
        if ($request->ajax()) {
            $ward = Ward::find($request->id);
            return response()->json($ward);
        }
    }
    public function getSubCategories(Request $request)
    {
        if ($request->ajax()) {
            $subcategories = Category::find($request->id)->subcategories;
            return response()->json($subcategories);
        }
    }

    public function getListingCoordinates(Request $request)
    {
        if ($request->ajax()) {
            $businessinformation = BusinessInformation::find($request->id);
            return response()->json($businessinformation);
        }
    }

}
