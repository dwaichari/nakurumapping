<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    public function businessinformations()
    {
        return $this->hasMany(BusinessInformation::class,'category_id');
    }
}
