<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function businessinformation()
    {
        return $this->belongsTo(BusinessInformation::class);
    }
}